package com.mycompany.ejercicionro6;
public class stackDinamicOne {
    char arr[];
    int top;
    
    stackDinamicOne(){
        this.arr = new char[5];
        this.top = -1;
    }
    public boolean isEmpty(){
        return (top == -1);
    }
    public void push(char element){
        top++;
        if(top < arr.length){
            arr[top] = element;
        }else{
            char temp[] = new char[arr.length+5];
            for(int i = 0; i<arr.length; i++){
                temp[i] = arr[i];
            }
            arr = temp;
            arr[top] = element;
        }
    }
    public char peek(){
        return arr[top];
    }
    public char pop(){
        if(!isEmpty()){
            int temptop = top;
            top--;
            char returntop = arr[temptop];
            return returntop;
        }else{
            return '-';
        }
    }
    
}
